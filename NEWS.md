# bonepairs 0.9.3 (Release date: 2019-08-02)

* A new argument `priorSame` in `pm_qda_*` functions now allows to set custom priors in QDA models.
* The function `pm_plot_ROC` was modified to take into account a recent update from the R package `pROC`.
* Improved comments and fixed indentation in R source files.
* Improved examples in some documentation files.

# bonepairs 0.9.2 (Release date: 2019-05-15)

## Major changes

* Complete redesign of the API, in particular with three separate functions (LOO, test and auto) for each method implemented
* More efficient handling of outliers in get_GDS function

## Other changes

* New default values for get_GDS function, to be consistent with an upcoming article presenting the package (Santos & Villotte, 2019)
* Updated documentation with minimal working examples in all Rd files
* Various bug fixes

# bonepairs 0.5.0 (Release date: 2019-04-03)

* First release on GitLab
