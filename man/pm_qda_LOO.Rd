\name{pm_qda_LOO}
\alias{pm_qda_LOO}
\title{
  Build an osteometric sorting model based on quadratic discriminant
  analysis, and evaluate its accuracy in LOOCV
}
\description{
  Implements a method of pair-matching described by Santos and Villotte
  (2019). This function evaluates and returns a QDA model for
  pair-matching using a learning sample provided as input.
}
\usage{
pm_qda_LOO(learningL, learningR, sitesLearning = NULL, level = 0.1,
           priorSame = 0.5)
}
\arguments{
  \item{learningL}{
    a dataframe (for which rownames are mandatory) giving the
    measurements of left bones from a learning sample.
  }
  \item{learningR}{
    a dataframe (for which rownames are mandatory) giving the
    measurements of right bones from a learning sample.
  }
  \item{sitesLearning}{
    either \code{NULL}, or a factor. If a factor is given (typically a
    factor indicating geographical or chronological groups among the
    learning sample), the bone pairs evaluated are only created for each
    level of this factor. (I.e., a bone from the population sample
    \sQuote{A} will never be compared with a bone from the population
    sample \sQuote{B}, but only with other bones from sample
    \sQuote{A}. See Santos & Villotte 2019 for more details.)
  }
  \item{level}{
    numeric value between 0 and 1. Alpha-level used for the decision
    rule of exclusion.
  }
  \item{priorSame}{
    numeric value between 0 and 1. Allow for custom priors in QDA
    models. By default, the two classes \dQuote{these bones might belong
    to the same individual} and \dQuote{these bones come from distinct
    individuals} have the same prior probability, 0.50. One can set here
    a prior probability \eqn{p} for the class \dQuote{these bones might
    belong to the same individual}, and the complement \eqn{1-p} is
    automatically set for the other class.
  }
}
\note{
  \code{learningL} and \code{learningR} must have the same attributes
  (dimensions, colnames and rownames) so that a model can be trained on
  true pairs of skeletal elements.
}
\value{
  A list of four elements:
  \item{indivResCV}{dataframe. Results of pair-matching obtained in
    LOOCV on the learning sample.}
  \item{qdaModel}{an R object of type \code{qda}, build using the
    learning sample.}
  \item{TPR}{numeric value. True positive rate computed in LOOCV.}
  \item{FPR}{numeric value. False positive rate computed in LOOCV.}
}
\seealso{
  \code{\link[MASS]{qda}}
}
\author{
  Frédéric Santos, \email{frederic.santos@u-bordeaux.fr}
}
\references{
  Santos, F. and Villotte, S. (2019) Using quadratic discriminant
  analysis for osteometric pair-matching of long bone antimeres: An
  evaluation on modern and archaeological samples. Submitted to
  \emph{International Journal of Osteoarchaeology}.
}
\examples{
\dontrun{
## Import a dataframe for femoral measurements, and discard some outliers:
femur <- get_GDS(bone = "femur", summarize = FALSE,
                 threshold.outliers = 4)
## Keep apart geographical information for the learning dataset:
trainOrig <- femur$Modern$NOTE
## Create separate dataframes for left and right femora:
trainL <- train[ , -grep("R", colnames(train))] # left femur
trainR <- train[ , grep("R", colnames(train))] # right femur
## Predictions in LOOCV on the training sample:
res <- pm_qda_LOO(learningL = trainL, learningR = trainR, 
                  sitesLearning = trainOrig, level = 0.1)
## Summarize results for the training sample (LOOCV):
pm_results(res$indivResCV)
}
}
