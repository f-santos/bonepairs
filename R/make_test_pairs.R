make_test_pairs <- function(tabL, tabR, type=c("classifier", "reg", "ttest")) {
 # Internal function to create a complete dataframe of bone pairs, as required by some portions of the R code from pm.rf, pm.cca, etc.
 # tabL & tabR: dataframes for left and right bones respectively (should have the same dimensions and dimnames)
   
  pairs <- expand.grid(Bone_L=rownames(tabL), Bone_R=rownames(tabR)) # 2-columns dataframe indicating all possible bone pairs (useful for all 'types' below)
  
  if (type=="classifier" | type=="reg") { # generate dataframes suitable for pm.qda and pm.rf functions
    dataPairs <- matrix(NA, ncol=ncol(tabL)+8, nrow=nrow(tabL)*nrow(tabR)) # initialize the dataframe of bone pairs...
    colnames(dataPairs) <- c("Bone_L", "Bone_R", colnames(tabL), "Answer", "pval", "Variables_used", "N_learning_bones", "TPR_learning", "FPR_learning") # ... and give it proper column names...
    rownames(dataPairs) <- paste(pairs$Bone_L, pairs$Bone_R, sep=" / ") # and row names.
    dataPairs <- as.data.frame(dataPairs)
    dataPairs[ , c("Bone_L", "Bone_R")] <- pairs
    dataPairs[ , colnames(tabL)] <- tabL[pairs$Bone_L,] - tabR[pairs$Bone_R,] # compute the differences (left-right measurements) for each bone pair and add them to the dataframe
    return(dataPairs)
  } 
}
