pm_logreg_LOO <- function(learningL, learningR, sitesLearning=NULL, level=0.1, graph=FALSE, progressbar=TRUE) {
 # Function to build a linear model for pair-matching and to perform a LOOCV on it.
 # learningL: learning dataset (dataframe) for left bone values
 # learningR: learning dataset (dataframe) for right bone values
 # (learningL and learningR must have the same dimensions, rownames and colnames)
 # sitesLearning: either NULL, either a grouping factor indicating archaeological sites in the learning sample. In the latter case, bone pairs are created *within each site*, and not in the whole sample.
 # level: by default 0.1; alpha-level for pair-matching model
 # graph: boolean (display a graph or not)
 # progressbar: boolean (displays a progress bar or not)
 
  ################################################################
  # 1. Complete model (learning step, using "true associations") #
  ################################################################
  # 1.1. Compute the log-transformed variables:
  xcoor <- log(rowSums(learningL)) # coordinates of bone pairs along the X-axis (left bones)
  ycoor <- log(rowSums(learningR)) # coordinates of bone pairs along the Y-axis (right bones)

  # 1.2. Build linear regression model:
  df <- data.frame(xcoor=xcoor, ycoor=ycoor) # learning data: log-sum coordinates of individuals
  reg <- lm(ycoor ~ xcoor, data=df) # the "complete" regression model, learned on true bone pairs, that predicts right bone values using left bone values.
  N <- length(xcoor) # number of specimens used to build the complete model
  regSE <- summary(reg)$sigma # Residual standard error
  
  # 1.3. Graph (if requested by the user):
  if (graph==TRUE) {
    plot(xcoor, ycoor, main="Biplot for Byrd and Adams' (2003) regression model", pch=16, asp=1, xlab="LogSum(X)", ylab="LogSum(Y)")
    abline(reg, col="blue", lwd=1.2) # add regression line
    grilleX <- seq(from=min(xcoor), to=max(xcoor), length=100) # the horizontal axis is subdivided into 100 equal parts
    grille <- data.frame(grilleX) # we turn grilleX into a data frame, because the 'predict' function will require this data structure
    colnames(grille) <- "xcoor" # and we have to give it a proper 'colnames', since the 'predict' function also needs it.
    p.pred <- predict(reg, newdata=grille, interval="prediction", level=1-level) # values for the confidence interval
    matlines(grilleX, p.pred[,2:3], lty=c(3,3), lwd=c(2,2), col="blue") # display the confidence interval with blue dotted lines
  }
  
  #######################################################
  # 2. Cross-validation (LOOCV) on the learning dataset #
  #######################################################
  # 2.1. Create a dataframe of all possible associations (true or false) to be evaluated:
  MatCV <- make_pairs(learningL, learningR, type="reg") # we create all possible bone pairs within the whole sample.
  MatCV$xcoor <- rep(xcoor, times=length(ycoor))
  MatCV$ycoor <- rep(ycoor, each=length(xcoor))
  if (!is.null(sitesLearning)) { # if bone pairs have to be made withinh each archaelogical site,
    tokeep <- ifelse(sitesLearning[MatCV$Bone_L]==sitesLearning[MatCV$Bone_R], TRUE, FALSE)# boolean, indicates if each bone pair is to be kept (because it comes from the same site) or removed (because it comes from two different sites)
    MatCV <- MatCV[tokeep, ] # keep only the pairs coming from the same site
  }
  # Now, MatCV lists all possible associations between a left bone and a right bone in the reference datasets.
  
  # 2.2. Compute results in LOOCV:
  if (progressbar) {
    pb <- progress_bar$new(format="LOOCV on current model [:bar] :percent eta: :eta", total=nrow(MatCV), clear=TRUE, width=60) # display a progress bar on the R console
  }
  for (i in 1:nrow(MatCV)) { # for each possible association of two bones,
    leftbone <- as.character(MatCV[i, "Bone_L"]) # id of considered left bone
    rightbone <- as.character(MatCV[i, "Bone_R"]) # id of considered right bone
    reg.temp <- lm(ycoor ~ xcoor, data=df[!rownames(df) %in% c(leftbone, rightbone), ]) # compute a regression model without these two bones
    N.temp <- nrow(reg.temp$model) # number of specimens used to build this CV model
    MatCV[i, "N_learning_bones"] <- N.temp
    regSE.temp <- summary(reg.temp)$sigma # Residual standard error
    newvalues <- MatCV[MatCV$Bone_L==leftbone & MatCV$Bone_R==rightbone, ] # retrieve the X and Y values (i.e., log-transformed sum of measurements) for these two bones
    MatCV[i, "fit"] <- predict(reg.temp, newdata=newvalues) # and use the regression model to predict them.
    MatCV[i, "t"] <- abs(MatCV[i, "ycoor"] - MatCV[i, "fit"]) / (regSE.temp * sqrt(1 + 1/N.temp + (MatCV[i, "xcoor"]-mean(xcoor[-i]))^2/(N.temp*var(xcoor[-i])))) # associated t-values (cf. Cornillon & Matzner-Løber p. 20, or Byrd 2008 p. 206)
    MatCV[i, "pval"] <- 2 * (1-pt(MatCV[i, "t"], df=N.temp-2))
    if (progressbar) {
      pb$tick()
    }
  }
  MatCV$Answer <- factor(ifelse(MatCV$pval<level, "Rejected association", "Possible match"))
  MatCV$Measurements_Used <- factor(rep(paste0(colnames(learningL), collapse=", "), nrow(MatCV)))

  #########################
  # 3. Quality indicators #
  #########################
  quality.indicators <- pm_results(MatCV, level=level, msg=FALSE)
    
  #####################
  # 4. Return results #
  #####################
  return(list(indivResCV=MatCV, logregModel=reg, TPR=quality.indicators$TPR, FPR=quality.indicators$FPR))
}
