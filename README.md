bonepairs 
=========

This R package is still in an early phase of development. The API may change in a substantial manner in the next versions.

## Installation of the R package `bonepairs`

### Install prerequisites

1. Install the R package `devtools` by typing the following command line into the R console:

		install.packages("devtools", dep = TRUE)

2. Install build environment:
    * **Windows:** Install latest version of *[Rtools](https://cran.r-project.org/bin/windows/Rtools/)*. During the installation process, make sure to select *"Edit the system path"*.
    * **OSX:** Install *[XCODE](https://developer.apple.com/xcode/)*

### Installing `bonepairs`

Run the following command in R:
        
	devtools::install_git('https://gitlab.com/f.santos/bonepairs.git')

## Citing `bonepairs`

Citation information can be obtained by typing:

	citation("bonepairs")

into the R console.
